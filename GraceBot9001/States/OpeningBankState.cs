﻿using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.Misc;
using AOSharp.Core.UI;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraceBot9001.States
{
    internal static class OpeningBankState
    {
        internal static AutoResetInterval BankOpenRetryInterval = new AutoResetInterval(500);

        internal static void Run()
        {
            if (Inventory.Bank.IsOpen)
            {
                Main.StateMachine.Fire(Trigger.BankOpened);
                return;
            }

            if (!BankOpenRetryInterval.Elapsed)
                return;

            if (Inventory.Find(Main.PORTABLE_BANK_ID, out Item portableBank))
            {
                portableBank.Use();
                return;
            }

            if (DynelManager.Find("Bank", out SimpleItem bankTerminal))
            {
                bankTerminal.Use();
                return;
            }

            Main.FatalError("Unable to locate a bank source.");
        }
    }
}
