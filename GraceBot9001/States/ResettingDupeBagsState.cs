﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.GMI;
using AOSharp.Core.Inventory;
using AOSharp.Core.Misc;
using AOSharp.Core.UI;
using GraceBot9001.IPCMessages;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraceBot9001.States
{
    internal static class ResettingDupeBagsState
    {
        private static AutoResetInterval _interval = new AutoResetInterval(500);

        internal static void Run()
        {
            if (!_interval.Elapsed)
                return;

            Backpack dupeBag;

            if (Inventory.Bank.FindContainer("DupeBag", out dupeBag))
            {
                Item.MoveItemToInventory(dupeBag.Slot);
                return;
            }

            if (Inventory.FindContainer("DupeBag", out dupeBag) && Inventory.FindContainer("ShopFood", out Backpack shopFoodBag))
            {
                if (!dupeBag.IsOpen)
                {
                    Item.Use(dupeBag.Slot);
                    return;
                }

                if (!shopFoodBag.IsOpen)
                {
                    Item.Use(shopFoodBag.Slot);
                    return;
                }

                foreach (Item item in dupeBag.Items)
                    item.MoveToContainer(shopFoodBag);

                foreach (Item item in shopFoodBag.Items)
                    item.MoveToContainer(dupeBag);

                Main.StateMachine.Fire(Trigger.DupeBagsReset);
                
                Task.Delay(2000).ContinueWith((e) => Main.IPCChannel.Broadcast(new DupeItemsReadyMessage()));
            }
        }
    }
}
