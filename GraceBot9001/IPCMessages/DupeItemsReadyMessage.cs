﻿using AOSharp.Core.IPC;
using SmokeLounge.AOtomation.Messaging.Serialization.MappingAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraceBot9001.IPCMessages
{
    [AoContract((int)IPCOpcode.DupeItemsReady)]
    public class DupeItemsReadyMessage : IPCMessage
    {
        public override short Opcode => (int)IPCOpcode.DupeItemsReady;
    }
}
