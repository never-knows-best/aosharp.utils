﻿using AOSharp.Core.UI;
using Newtonsoft.Json;
using org.critterai.nav;
using org.critterai.nmgen;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace NavGen
{
   
    public class Config
    {
        public static string Path = $"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\NavGen";

        public NMGenParams NMGenParams { get; set; }

        public static Config Load()
        {
            Config config;
            try
            {
                config = JsonConvert.DeserializeObject<Config>(File.ReadAllText(Path));
            }
            catch
            {
                Chat.WriteLine($"No config file found.");
                Chat.WriteLine($"Using default settings");
                config = new Config
                {
                    NMGenParams = new NMGenParams
                    {
                        XZCellSize = 0.1f,
                        YCellSize = 0.07f,
                        MaxVertsPerPoly = 3,
                        MaxEdgeLength = 10,
                        EdgeMaxDeviation = 5,
                        TileSize = 512,
                        MergeRegionArea = 1,
                        WalkableSlope = 60,
                        maxEdgeLength = 10,
                        edgeMaxDeviation = 5f,
                    }
                };

                config.NMGenParams.SetWalkableHeight(1);
                config.NMGenParams.SetWalkableRadius(0.5f);
                config.NMGenParams.SetWalkableStep(0.3f);
            }

            return config;
        }

        public static void CreateDirectory()
        {
            Directory.CreateDirectory($"{Path}\\Navmeshes");
        }

        private void SaveNavMesh(NavmeshMetaData metaData, Navmesh navmesh)
        {
            BinaryFormatter formatter = new BinaryFormatter();

            using (var inflateStream = new FileStream(metaData.Name, FileMode.CreateNew))
                formatter.Serialize(inflateStream, navmesh.GetSerializedMesh());
        }

        public static Navmesh LoadNavmesh(string name)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = null;
            string filePath = $"{Path}\\Navmeshes\\name.Navmesh";

            if (!File.Exists(filePath))
                return null;

            try
            {
                stream = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read);
                NavStatus status = Navmesh.Create((byte[])formatter.Deserialize(stream), out Navmesh navMesh);
                if (status == NavStatus.Sucess)
                {
                    return navMesh;
                }
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            return null;
        }

        public class NavmeshMetaData
        {
            public string Name;
            public int PlayfieldId;
        }
    }
}
