﻿using AOSharp.Common.GameData.UI;
using AOSharp.Common.GameData;
using AOSharp.Core.UI;
using AOSharp.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AOSharp.Core.Misc;
using AOSharp.Core.Inventory;
using System.Runtime.InteropServices.ComTypes;

namespace Qubit
{
    public class UsablesWindow
    {
        private Window _window;
        private string _xmlPath;

        private List<Dynel> _dynelCache;
        private ViewCache _viewCache;
        private Vector3? _targetPosition;
        private AutoResetInterval _tickInterval = new AutoResetInterval(500);

        public UsablesWindow(string xmlPath)
        {
            _xmlPath = xmlPath;
            _viewCache = new ViewCache();
        }

        public void Open()
        {
            if (_window != null && _window.IsValid)
                return;

            WindowFlags winFlags = WindowFlags.AutoScale | WindowFlags.NoFade;
            _window = Window.CreateFromXml("Qubit", _xmlPath, windowSize: new Rect(0, 0, 400, 500), windowStyle: WindowStyle.Default, windowFlags: winFlags);

            if (_window == null)
            {
                Chat.WriteLine("Failed to create config window.");
                return;
            }

            CreateMultiListView("ItemViewContainer");

            if (_window.FindView("InteractButton", out _viewCache.InteractButton))
                _viewCache.InteractButton.Clicked += (o, e) => Interact();

            _window.Show(true);
        }

        public void OnTeleport()
        {
            var itemsToRemove = _viewCache.ItemListView.Items.ToList();

            foreach (var item in itemsToRemove)
                _viewCache.ItemListView.RemoveItem(item);

            _dynelCache.Clear();
        }

        public void Update()
        {
            try
            {
                if (Game.IsZoning)
                    return;

                if (_window == null || !_window.IsValid)
                    return;

                if (_targetPosition.HasValue)
                    Debug.DrawLine(DynelManager.LocalPlayer.Position, _targetPosition.Value, DebuggingColor.Green);

                if (!_tickInterval.Elapsed)
                    return;

                var dynels = GetFilteredDynels();

                SetTargetPosition(dynels);

                var dynelsToRemove = _dynelCache.Except(dynels).Select(x => x.Identity);
                var dynelsToAdd = dynels.Except(_dynelCache);

                var itemsToRemove = _viewCache.ItemListView.Items.Where(x => dynelsToRemove.Contains(((Identity)x.Tag))).ToList();

                foreach (var item in itemsToRemove)
                {
                    _viewCache.ItemListView.RemoveItem(item);
                    //Chat.WriteLine($"Removing item: {item.Tag}");
                }

                foreach (var dynel in dynelsToAdd)
                {
                    AddDynelItem(dynel);
                    //Chat.WriteLine($"Adding item: {dynel.Identity}");
                }

                _dynelCache = dynels;
            }
            catch (Exception ex)
            {
                Chat.WriteLine(ex.Message);
            }
        }

        private List<Dynel> GetFilteredDynels()
        {
            IdentityType[] typeBlacklist = new IdentityType[]
            {
                IdentityType.PlayfieldUnk,
                IdentityType.Container,
                IdentityType.MissionKey,
                IdentityType.MissionKeyDuplicator,
            };

            return Utils.GetDynels().Where(x => !typeBlacklist.Contains(x.Identity.Type) && ((CanFlags)x.GetStat(Stat.Can)).HasFlag(CanFlags.Use)).ToList();
        }

        private void Interact()
        {
            var selectedItem = _viewCache.ItemListView.Items.FirstOrDefault(x => x.IsSelected);

            if (selectedItem == null)
                return;

            Item.Use((Identity)selectedItem.Tag);
        }

        private void SetTargetPosition(List<Dynel> dynels)
        {
            var selectedItem = _viewCache.ItemListView.Items.FirstOrDefault(x => x.IsSelected);

            if (selectedItem == null)
            {
                _targetPosition = null;
                return;
            }

            var targetDynel = dynels.FirstOrDefault(x => x.Identity == (Identity)selectedItem.Tag);

            if (targetDynel == null)
            {
                _targetPosition = null;
                return;
            }

            _targetPosition = targetDynel.GlobalPosition;
        }

        private void RefreshItems()
        {
            _dynelCache = GetFilteredDynels();
            foreach (var dynel in _dynelCache)
                AddDynelItem(dynel);
        }

        private void AddDynelItem(Dynel dynel)
        {
            if (DummyItem.CreateDummyItemID(dynel.GetStat(Stat.ACGItemTemplateID), dynel.GetStat(Stat.ACGItemTemplateID2), dynel.GetStat(Stat.ACGItemLevel), out Identity dummyItemId))
            {
                InventoryListViewItem invItem = InventoryListViewItem.Create(0, dummyItemId, true);
                invItem.Tag = dynel.Identity;
                _viewCache.ItemListView.AddItem(_viewCache.ItemListView.GetFirstFreePos(), invItem, true);
            } 
        }

        private void CreateMultiListView(string containerName)
        {
            if (_window.FindView(containerName, out View containerView))
            {
                _viewCache.ItemListView = ItemListViewBase.Create(new Rect(), 0x40, 0xF);
                _viewCache.ItemListView.SetLayoutMode(1);
                _viewCache.ItemListView.AddColumn(0, "Icon", 30);
                _viewCache.ItemListView.AddColumn(1, "Name", 350);
                _viewCache.ItemListView.ItemSelectionStateChanged += ItemSelectionStateChanged;

                containerView.AddChild(_viewCache.ItemListView, false);
                RefreshItems();
            }
        }

        private void ItemSelectionStateChanged(object obj, bool selected)
        {
            _viewCache.InteractButton.Enable(_viewCache.ItemListView.HasSelection);
        }

        public class ViewCache
        {
            public ItemListViewBase ItemListView;
            public Button InteractButton;
        }
    }
}
