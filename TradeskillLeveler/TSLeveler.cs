﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.Misc;
using AOSharp.Core.UI;
using SmokeLounge.AOtomation.Messaging.GameData;
using SmokeLounge.AOtomation.Messaging.Messages;
using SmokeLounge.AOtomation.Messaging.Messages.N3Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using static AOSharp.Core.Battlestation;

namespace TradeskillLeveler
{
    public enum TSLevelerMode
    {
        Implant,
        NCU
    }

    public class TSLeveler : AOPluginEntry
    {
        public static int DesiredLevel;
        public static int PacketsPerTick = 1;
        public static TSLevelerMode Mode = TSLevelerMode.Implant;
        public static bool ResearchMode = false;
        public static TSLeveler Instance;

        private bool _enabled = false;

        private bool _waitingOnLevel = false;

        private double _lastXpTick = 0;
        private bool _stillReceivingXp => Time.NormalTime < _lastXpTick + 10 && _remainingXpTicks > 0 || CachedXp > DynelManager.LocalPlayer.GetStat(Stat.NextXP);

        private AutoResetInterval _sideXpUpdateInterval = new AutoResetInterval(5000);
        private double _lastSideXpResult = 0;
        private bool _sideXpUpdated => _sideXp > 0 && Time.NormalTime < _lastSideXpResult + 30;
        private double _sideXp = 0;

        private int _remainingTradeskills = 0;
        private int _remainingXpTicks = 0;

        private int? _cachedXp;
        private int CachedXp => _cachedXp.HasValue ? _cachedXp.Value : DynelManager.LocalPlayer.GetStat(Stat.XP);

        private ItemCache _itemCache = new ItemCache();
        private TradeSkillLevelerWindow _window;
        private TradeskillPair _tradeskillPair = null;

        private List<string> _logs = new List<string>();

        public override void Run(string pluginDir)
        {
            Mode = DynelManager.LocalPlayer.Profession == Profession.Shade ? TSLevelerMode.NCU : TSLevelerMode.Implant;

            Chat.RegisterCommand("fillbar", (string command, string[] param, ChatWindow chatWindow) =>
            {
                try
                {
                    if (_itemCache.MinTradeskillItem == null)
                        _itemCache.LoadItems();

                    TradeskillPair ql1TradeskillPair = _itemCache.Ql1TradeskillPair;
                    int tradeskills = (int)((float)XpToLevel() / GetTradeskillXpMin(ql1TradeskillPair)) + 1;
                    
                    for (int i = 0; i < tradeskills; i++)
                        Tradeskill(ql1TradeskillPair);
                } 
                catch { }
            });

            Chat.RegisterCommand("fillbarpool", (string command, string[] param, ChatWindow chatWindow) =>
            {
                try
                {
                    if (_itemCache.HighTradeskillItem == null)
                        _itemCache.LoadItems();

                    TradeskillPair highTradeskillPair = _itemCache.HighestTradeskillPair;
                    int tradeskills = (int)((float)XpToLevel() / GetTradeskillXpMin(highTradeskillPair)) + 1;

                    for (int i = 0; i < tradeskills; i++)
                        Tradeskill(highTradeskillPair);
                }
                catch { }
            });

            Chat.RegisterCommand("showlog", (string command, string[] param, ChatWindow chatWindow) =>
            {
                foreach (string log in Enumerable.Reverse(_logs).Take(50).Reverse().ToList())
                    Chat.WriteLine(log);
            });

            Chat.RegisterCommand("test", (string command, string[] param, ChatWindow chatWindow) =>
            {
                try
                {
                    if (_itemCache.LowTradeskillItem == null)
                        _itemCache.LoadItems();

                    Tradeskill(_itemCache.HighestTradeskillPair);
                }
                catch { }
            });

            _window = new TradeSkillLevelerWindow(pluginDir, _itemCache);
            _window.ActionButtonClicked += ActionButtonClicked;
            _window.Show();

            Game.OnUpdate += OnUpdate;
            Network.N3MessageReceived += Network_N3MessageReceived;
            Instance = this;
        }

        private void ActionButtonClicked(TradeSkillLevelerWindow.ButtonState state)
        {
            if (state == TradeSkillLevelerWindow.ButtonState.Start)
                Start();
            else if (state == TradeSkillLevelerWindow.ButtonState.Stop)
                Stop();
        }

        public void Start()
        {
            try
            {
                _itemCache.LoadItems();
            }
            catch (MissingItemException e)
            {
                Chat.WriteLine(e.Message);

                return;
            }

            if (ResearchMode)
                DisableXp();

            _enabled = true;
            _window.SetActionButtonState(TradeSkillLevelerWindow.ButtonState.Stop);
        }

        private void Stop()
        {
            _enabled = false;
            _window.SetActionButtonState(TradeSkillLevelerWindow.ButtonState.Start);
        }

        public void SetDesiredLevel(int desiredLevel)
        {
            DesiredLevel = desiredLevel;
            _window.SetDesiredLevel(desiredLevel);
        }

        private void OnUpdate(object s, float deltaTime)
        {
            _window.Update();

            if (!_enabled)
                return;

            if (ResearchMode)
            {
                for (int i = 0; i < PacketsPerTick; i++)
                    Tradeskill(_itemCache.HighestTradeskillPair);

                return;
            }

            if (DynelManager.LocalPlayer.Level >= DesiredLevel)
            {
                Log("Reached our desired level.");
                Stop();

                return;
            }

            if (_sideXpUpdateInterval.Elapsed)
            {
                Network.Send(new CharacterActionMessage 
                { 
                    Action = CharacterActionType.InfoRequest, 
                    Target = DynelManager.LocalPlayer.Identity 
                });
            }

            if (_remainingTradeskills <= 0 && !_waitingOnLevel && !_stillReceivingXp && _sideXpUpdated)
            {
                Log($"XPTimeout: {Time.NormalTime < _lastXpTick + 10} | _remainingXpTicks 0: {_remainingXpTicks} | XP Overage: {CachedXp > DynelManager.LocalPlayer.GetStat(Stat.NextXP)}");

                TradeskillPair highTradeskillPair = _itemCache.HighestTradeskillPair;

                int jumpXpMin = GetTradeskillXpMin(highTradeskillPair);
                int jumpXpMax = GetTradeskillXpMax(highTradeskillPair);

                if (XpToLevelNoPool <= jumpXpMin)
                {
                    Stop();

                    Log("We made a mistake somewhere. Level manually and restart.");
                    return;
                }

                //Low tradeskill count race condition fix
                _lastXpTick = Time.NormalTime;

                if (XpToLevelNoPool <= jumpXpMax)
                {
                    Log("Finishing Level");
                    DisableXp();
                    Tradeskill(highTradeskillPair);
                    EnableXp();
                    Tradeskill(highTradeskillPair);
                    _waitingOnLevel = true;

                    return;
                }

                //Try High XP tradeskill
                int tradeskills = (int)((float)XpToLevel() / jumpXpMin) - 1;

                int projectedXp = CurrentXp + UnsavedXp + tradeskills * jumpXpMin;

                if (TotalXpToLevel - projectedXp <= jumpXpMax)
                    tradeskills--;

                if (tradeskills > 0)
                {
                    _remainingTradeskills = tradeskills;
                    _remainingXpTicks = tradeskills;
                    _tradeskillPair = highTradeskillPair;
                    numXpTicks = 0;

                    int projectedXpGain = tradeskills * jumpXpMin + UnsavedXp;
                    projectedXp = CurrentXp + UnsavedXp + tradeskills * jumpXpMin;
                    int projectedTotalXp = CachedXp + projectedXpGain;
                    Log($"Using item {_tradeskillPair.Target.Name} {tradeskills} times. SideXP: {_sideXp}, TSXP: {jumpXpMin}, Unsaved XP: {UnsavedXp}, ProjectedXpGain: {projectedXpGain}, Projected Total XP: {projectedTotalXp}, Projected Relative XP: {projectedXp}, XpToLevel: {TotalXpToLevel}");
                    return;
                }

                //Try Low XP Tradeskill
                TradeskillPair ql1TradeskillPair = _itemCache.Ql1TradeskillPair;
                tradeskills = (int)(((float)XpToLevel() - jumpXpMax) / GetTradeskillXpMin(ql1TradeskillPair)) + 1;

                if (tradeskills > 0)
                {
                    _remainingTradeskills = tradeskills;
                    _remainingXpTicks = tradeskills;
                    _tradeskillPair = ql1TradeskillPair;
                    numXpTicks = 0;

                    Log($"Using item {_tradeskillPair.Target.Name} {tradeskills} times");
                    return;
                }
            }

            if (_remainingTradeskills > 0)
            {
                for (int i = 0; i < Math.Min(_remainingTradeskills, PacketsPerTick); i++)
                    Tradeskill(_tradeskillPair);
            }
        }

        private int numXpTicks = 0;

        private void Network_N3MessageReceived(object s, N3Message n3Msg)
        {
            if (n3Msg is StatMessage statMessage && statMessage.Identity == DynelManager.LocalPlayer.Identity && statMessage.Stats.Any(x => x.Value1 == Stat.XP))
            {
                _lastXpTick = Time.NormalTime;
                _remainingXpTicks--;
                _cachedXp = (int)statMessage.Stats.First(x => x.Value1 == Stat.XP).Value2;

                //Log($"Xp({++numXpTicks}): {statMessage.Stats.First(x => x.Value1 == Stat.XP).Value2} Client: {CachedXp}");
            }

            if (n3Msg is InfoPacketMessage infoPacketMsg && infoPacketMsg.Identity == DynelManager.LocalPlayer.Identity)
            {
                double sideXp = ((CharacterInfoPacket)infoPacketMsg.Info).SideXp / 100d;

                if (_sideXp > 0 && _sideXp != sideXp)
                    Log($"SideXp changed from {_sideXp} to {sideXp}");

                _sideXp = sideXp;
                _lastSideXpResult = Time.NormalTime;
            }

            if (n3Msg is NewLevelMessage newLevelMessage && newLevelMessage.Identity == DynelManager.LocalPlayer.Identity)
            {
                _waitingOnLevel = false;
                Log("WaitingOnLevel False");
            }
        }
        
        private void Tradeskill(TradeskillPair tradeskillPair)
        {
            Network.Send(new CharacterActionMessage
            {
                Action = CharacterActionType.UseItemOnItem,
                Identity = DynelManager.LocalPlayer.Identity,
                Target = tradeskillPair.Source.Slot,
                Parameter1 = (int)IdentityType.Inventory,
                Parameter2 = tradeskillPair.Target.Slot.Instance
            });

            _remainingTradeskills--;
        }

        private void EnableXp()
        {
            Network.Send(new CharacterActionMessage
            {
                Action = CharacterActionType.DisableXP,
                Parameter2 = 5,
                Unknown = 0,
            });
        }

        private void DisableXp()
        {
            Network.Send(new CharacterActionMessage
            {
                Action = CharacterActionType.DisableXP,
                Parameter2 = 21,
                Unknown = 0,
            });
        }

        private int GetTradeskillXpMin(TradeskillPair tsPair, bool withSidexp = true)
        {
            return Math.Min(XpCap, withSidexp ? (int)Math.Floor(tsPair.Target.QualityLevel * 5 * (1 + _sideXp / 100)) : tsPair.Target.QualityLevel * 5);
        }

        private int GetTradeskillXpMax(TradeskillPair tsPair)
        {
            return GetTradeskillXpMin(tsPair) + (tsPair.Target.QualityLevel * 5 / 2);
        }

        private int XpToLevelNoPool => DynelManager.LocalPlayer.GetStat(Stat.NextXP) - CachedXp;

        private int XpToLevel() => DynelManager.LocalPlayer.GetStat(Stat.NextXP) - CachedXp - DynelManager.LocalPlayer.GetStat(Stat.UnsavedXP);

        private int CurrentXp => CachedXp - DynelManager.LocalPlayer.GetStat(Stat.LastXP);

        private int TotalXpToLevel => DynelManager.LocalPlayer.GetStat(Stat.NextXP) - DynelManager.LocalPlayer.GetStat(Stat.LastXP);

        private int UnsavedXp => DynelManager.LocalPlayer.GetStat(Stat.UnsavedXP);

        private int XpCap => TotalXpToLevel / 10;

        private void Log(string message)
        {
            _logs.Add(message);
            Chat.WriteLine(message);
        }
    }

    public class TradeskillPair
    {
        public Item Source;
        public Item Target;

        public TradeskillPair(Item source, Item target)
        {
            Source = source;
            Target = target;
        }
    }
}
